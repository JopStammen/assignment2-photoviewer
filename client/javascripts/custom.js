/**
 * Created by Jop Stammen on 10-2-15.
 */

/*global photoviewer*/

(function () {
    "use strict";

    var ROWS = 4, COLS = 4, ID = "u9ymUX1fJLw", VOLUME = 0, PLAYBACKRATE = 1.0, HELP = 0, PROXY = "http://server7.tezzt.nl:1332/api/proxy";

    window.photoviewer = {

        tilesArray: [],
        tilesArrayRight: [],
        isPuzzleSolved : false,


        shuffle: function (array) {
            var  temp, index, counter = array.length;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = Math.floor(Math.random() * counter);

                // Decrease counter by 1
                counter--;

                // And swap the last element with it
                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }
            return array;
        },

        updateTiles: function () {
            var canvas, videoNode, tileBox, i, j, k, imgNode;
            imgNode = document.getElementById("emptyTile");
            j = 0;
            k = 0;
            videoNode = document.querySelector("#myVid");

            for (i = 0; i < window.photoviewer.tilesArray.length; i++) {
                if (j === COLS) {
                    j = 0;
                    k += 1;
                }
                tileBox = window.photoviewer.tilesArray[i];
                canvas = tileBox.getContext("2d");
                if (i === photoviewer.tilesArray.length - 1 && photoviewer.isPuzzleSolved == false) {
                    canvas.drawImage(imgNode, tileBox.width * j, tileBox.height * k, tileBox.width, tileBox.height, 0, 0, tileBox.width, tileBox.height);

                } else {
                    canvas.drawImage(videoNode, tileBox.width * j, tileBox.height * k, tileBox.width, tileBox.height, 0, 0, tileBox.width, tileBox.height);
                    if (HELP === 1) {
                        canvas.font = "16px Calibri";
                        canvas.fillText(i, 4, 16);
                        canvas.fillStyle = "red";
                    }
                }
                j++;
                if (photoviewer.arraysAreIdentical(photoviewer.tilesArray, photoviewer.tilesArrayRight) == true) {
                    photoviewer.isPuzzleSolved = true;
                    HELP = 0;
                }
            }

            setTimeout(photoviewer.updateTiles, 30);
        },

        arraysAreIdentical : function (a, b) {
            var i = a.length;
            if (i != b.length) { return false; }
            while (i--) {
                if (a[i] !== b[i]) { return false; }
            }
            return true;
        },

        createImage: function (bodyNode) {
            //Draw image
            var imgNode = document.createElement("img");
            imgNode.src = "./images/2x2.jpg";
            console.log(imgNode.src);
            imgNode.alt = "the empty tile";
            imgNode.id = "emptyTile";
            imgNode.style.display = "none";

            bodyNode.appendChild(imgNode);
        },

        createTiles: function () {
            var tileBox, tileContainer, rowCounter, columnCounter, tileName,
                tileWidth, tileHeight, bodyNode;

            tileWidth = Math.floor(document.querySelector("#myVid").offsetWidth / COLS);
            tileHeight = Math.floor(document.querySelector("#myVid").offsetHeight / ROWS);

            if (!document.querySelector("#tileContainer")) {
                bodyNode = document.querySelector("body");

                tileContainer = document.createElement("div");
                tileContainer.id = "tileContainer";
                tileContainer.className = "tileContainer";
                tileContainer.style.width = (Math.floor(document.querySelector("#myVid").offsetWidth) + 3 + COLS);
                tileContainer.style.height = (Math.floor(document.querySelector("#myVid").offsetHeight) + 3 + ROWS);

                bodyNode.appendChild(tileContainer);
            }

            for (rowCounter = 0; rowCounter < ROWS; rowCounter++) {
                for (columnCounter = 0; columnCounter < COLS; columnCounter++) {
                    tileName = "tile" + rowCounter + columnCounter;
                    if (!document.querySelector("#" + tileName)) {
                        tileBox = document.createElement("canvas");
                        tileBox.id = tileName;
                        tileBox.width = tileWidth;
                        tileBox.height = tileHeight;
                        tileBox.className = "tile";
                        tileContainer.appendChild(tileBox);
                        tileBox.addEventListener("click", photoviewer.clickHandler);

                        photoviewer.createImage(bodyNode);
                        photoviewer.tilesArray.push(tileBox);
                        photoviewer.tilesArrayRight.push(tileBox);
                    }
                }
            }
            photoviewer.shuffle(photoviewer.tilesArray);
            photoviewer.updateTiles();
        },

        switchTile: function (array, placeInArray) {
            var temp, radix = 10, index = photoviewer.tilesArray.length - 1, id = photoviewer.tilesArray[index].id, clickedTile = photoviewer.tilesArray[placeInArray].id, split = id.split("e"),
                splitClicked =  clickedTile.split("e"), number = parseInt(split[1], radix), numberClicked = parseInt(splitClicked[1], radix);

            if (number - 10 == numberClicked || number - 1 == numberClicked || number + 1 == numberClicked || number + 10 == numberClicked) {
                temp = array[placeInArray];
                array[placeInArray] = array[index];
                array[index] = temp;
                return array;
            }
            return array;
        },

        createVideoNode: function () {
            var bodyNode, videoNode, cb;

            //get bodyNode
            bodyNode = document.querySelector("body");

            //create video node
            videoNode = document.createElement("video");
            videoNode.id = "myVid";
            videoNode.volume = VOLUME;
            videoNode.loop = true;
            videoNode.autoplay = "autoplay";

            bodyNode.appendChild(videoNode);
            videoNode.addEventListener("loadeddata", photoviewer.createTiles);


            cb = function (video) {
                console.log(video.title);
                var webm = video.getSource("video/webm", "medium"), speed = ((PLAYBACKRATE > 2) ? 2 : PLAYBACKRATE && (PLAYBACKRATE < 0) ? 0.5 : PLAYBACKRATE);
                console.log("WebM: " + webm.url);
                videoNode.src = webm.url;
                videoNode.playbackRate = speed;
            };
            YoutubeVideo(ID, PROXY, cb);

        },


        clickHandler : function (e) {

            var currentTile = e.target, i;

            for (i = 0; i < photoviewer.tilesArray.length; i++) {
                if (currentTile == photoviewer.tilesArray[i]) {
                    if (photoviewer.arraysAreIdentical(photoviewer.tilesArray, photoviewer.tilesArrayRight) == false) {
                        photoviewer.switchTile(photoviewer.tilesArray, i);
                    }

                }
            }
        },

        init: function () {
            var h1Node, bodyNode, parameters, key, val, i;

            bodyNode = document.querySelector("body");
            h1Node = document.createElement("h1");
            h1Node.textContent = "Video sliding puzzle";
            bodyNode.appendChild(h1Node);

            if (parameters = window.location.href.split("?")[1]) {
                for (parameters = parameters.split("&"), i = 0; i < parameters.length; i++) {
                    key = parameters[i].split("=")[0];
                    val = parameters[i].split("=")[1];
                    key && (isNaN(val) ? eval(key + '="' + val + '"') : eval(key + "=" + val));
                }
            }
        },

        run: function () {
            photoviewer.init();
            photoviewer.createVideoNode();
        }
    };
}());

window.addEventListener("load", photoviewer.run);
