week 1:

    1. Started with only a video and a tilebox with some tiles

week 2:

    1. Filled the tiles with a video using the canvas.
    2. Added a clickhandler.
    3. Created an array and randomized it
    4. Added white tile

week 3:

    1. Added url functionality.
    2. Fixed jslint errors.