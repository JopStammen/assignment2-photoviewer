LintRoller v2.3.5

    Output for Wed Feb 25 2015 10:41:57 GMT+0100 (CET)

    Total files found   : 2
    All errors reported : 59


    308 lines checked via JSLINT
    308 lines checked via JSHINT
    308 lines checked via ESPRIMA

=============== Running JSLINT [ Total errors: 21 ] ===============

../../client/javascripts/custom.js
    Line #: 39
    Char #: 30
    Reason: Expected exactly one space between 'function' and '('.

../../client/javascripts/custom.js
    Line #: 39
    Char #: 32
    Reason: Expected exactly one space between ')' and '{'.

../../client/javascripts/custom.js
    Line #: 40
    Char #: 24
    Reason: Missing space between ',' and 'videoNode'.

../../client/javascripts/custom.js
    Line #: 46
    Char #: 16
    Reason: Missing space between 'for' and '('.

../../client/javascripts/custom.js
    Line #: 46
    Char #: 17
    Reason: Move 'var' declarations to the top of the function.

../../client/javascripts/custom.js
    Line #: 46
    Char #: 17
    Reason: Stopping. (26% scanned).

../../client/javascripts/youtube-video.js
    Line #: 39
    Char #: 15
    Reason: 'YoutubeVideo' was used before it was defined.

../../client/javascripts/youtube-video.js
    Line #: 41
    Char #: 20
    Reason: 'YoutubeVideo' was used before it was defined.

../../client/javascripts/youtube-video.js
    Line #: 41
    Char #: 83
    Reason: Expected ';' and instead saw '}'.

../../client/javascripts/youtube-video.js
    Line #: 51
    Char #: 9
    Reason: Unnecessary 'use strict'.

../../client/javascripts/youtube-video.js
    Line #: 59
    Char #: 27
    Reason: 'XDomainRequest' was used before it was defined.

../../client/javascripts/youtube-video.js
    Line #: 61
    Char #: 23
    Reason: 'XDomainRequest' was used before it was defined.

../../client/javascripts/youtube-video.js
    Line #: 52
    Char #: 18
    Reason: Unused 'ar'.

../../client/javascripts/youtube-video.js
    Line #: 52
    Char #: 22
    Reason: Unused 'attributeName'.

../../client/javascripts/youtube-video.js
    Line #: 52
    Char #: 37
    Reason: Unused 'params'.

../../client/javascripts/youtube-video.js
    Line #: 89
    Char #: 22
    Reason: 'YoutubeVideo' was used before it was defined.

../../client/javascripts/youtube-video.js
    Line #: 93
    Char #: 26
    Reason: Expected 'String' and instead saw ''''.

../../client/javascripts/youtube-video.js
    Line #: 94
    Char #: 21
    Reason: Expected 'String' and instead saw ''''.

../../client/javascripts/youtube-video.js
    Line #: 102
    Char #: 17
    Reason: 'YoutubeVideo' was used before it was defined.

../../client/javascripts/youtube-video.js
    Line #: 108
    Char #: 25
    Reason: 'YoutubeVideo' was used before it was defined.

../../client/javascripts/youtube-video.js
    Line #: 115
    Char #: 13
    Reason: The body of a for in should be wrapped in an if statement to filter unwanted properties from the prototype.

=============== Running JSHINT [ Total errors: 38 ] ===============

../../client/javascripts/custom.js
    Line #: 107
    Char #: 37
    Reason: Expected an assignment or function call and instead saw an expression.

../../client/javascripts/custom.js
    Line #: 145
    Char #: 13
    Reason: Missing 'new' prefix when invoking a constructor.

../../client/javascripts/custom.js
    Line #: 150
    Char #: 22
    Reason: Expected an assignment or function call and instead saw an expression.

../../client/javascripts/custom.js
    Line #: 170
    Char #: 6
    Reason: Missing semicolon.

../../client/javascripts/custom.js
    Line #: 35
    Char #: 13
    Reason: 'console' is not defined.

../../client/javascripts/custom.js
    Line #: 52
    Char #: 17
    Reason: 'console' is not defined.

../../client/javascripts/custom.js
    Line #: 59
    Char #: 24
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 76
    Char #: 81
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 77
    Char #: 83
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 85
    Char #: 109
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 86
    Char #: 111
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 91
    Char #: 46
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 92
    Char #: 54
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 102
    Char #: 59
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 103
    Char #: 25
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 104
    Char #: 25
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 107
    Char #: 25
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 112
    Char #: 13
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 112
    Char #: 33
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 117
    Char #: 13
    Reason: 'console' is not defined.

../../client/javascripts/custom.js
    Line #: 117
    Char #: 25
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 135
    Char #: 54
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 139
    Char #: 17
    Reason: 'console' is not defined.

../../client/javascripts/custom.js
    Line #: 141
    Char #: 17
    Reason: 'console' is not defined.

../../client/javascripts/custom.js
    Line #: 145
    Char #: 13
    Reason: 'YoutubeVideo' is not defined.

../../client/javascripts/custom.js
    Line #: 145
    Char #: 26
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 145
    Char #: 58
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 166
    Char #: 13
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 167
    Char #: 13
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 168
    Char #: 13
    Reason: 'photoviewer' is not defined.

../../client/javascripts/custom.js
    Line #: 174
    Char #: 5
    Reason: 'photoviewer' is not defined.

../../client/javascripts/youtube-video.js
    Line #: 41
    Char #: 83
    Reason: Missing semicolon.

../../client/javascripts/youtube-video.js
    Line #: 51
    Char #: 9
    Reason: Unnecessary directive "use strict".

../../client/javascripts/youtube-video.js
    Line #: 39
    Char #: 15
    Reason: 'YoutubeVideo' is not defined.

../../client/javascripts/youtube-video.js
    Line #: 41
    Char #: 20
    Reason: 'YoutubeVideo' is not defined.

../../client/javascripts/youtube-video.js
    Line #: 89
    Char #: 22
    Reason: 'YoutubeVideo' is not defined.

../../client/javascripts/youtube-video.js
    Line #: 102
    Char #: 17
    Reason: 'YoutubeVideo' is not defined.

../../client/javascripts/youtube-video.js
    Line #: 108
    Char #: 25
    Reason: 'YoutubeVideo' is not defined.

=============== Running ESPRIMA [ Total errors: 0 ] ===============

